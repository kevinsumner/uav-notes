# Tips

- Any time you touch a quad with a soldering iron, use a Vifly ShortSaver 2 the next time you put power to it to make sure there aren't any wiring or soldering mistakes.

- Before putting any solder on your flight controller, connect it to Betaflight Configurator to make sure it works. In particular, make sure the gyro works.

- Second, solder a pigtail to the ESC and get the ESC-FC wiring harness pinned and connected up between the ESC and the FC (or solder if the FC or ESC doesn't have a harness plug). Check that the ESC configuration software (e.g., BLHeliSuite32 or esc-configurator.com) can see and read all the ESCs.

- For at least your first two builds, do a wiring diagram. [Watch Bardwell's wiring diagram video](https://www.youtube.com/watch?v=APfDzb6fIoY) to learn how. After a few builds, you'll get intuition for wiring that lessens the need for a wiring diagram.

- Take battery safety seriously. LiPo fires burn down houses. Get a good, trustworthy charger. Keep packs at storage voltage. Never overcharge a pack. Use a Bat Safe or similar fire-proof case for keeping them in. Check batteries for bad cells regularly. Don't try to revive a dead battery with trickle charging. Discharge and dispose of suspect batteries.

- Buy quality parts. Avoid Skystars, Racerstar, and BetaFPV, to name a few. Matek, Aikon, T-Motor, and Diatone are generally safe bets for electronics. FPVCycle, iFlight, and T-Motor make good motors.

- Get a good soldering iron with temperature control. Pinecil is good and cheap, but stock is spotty at the moment. Hakko FX-888D is pricey but the standard for hobby bench soldering irons.

- Take some time to do a solder practice board. Diatone makes a good, cheap one.

- Use no-clean soldering flux pen. Use 63/37 leaded solder if it's available in your country -- US has it, EU has banned it I think.

- Try to spec your build as light as possible to meet your desires. Lighter quads can always be made heavier with a bigger battery. Heavier quads require a lot more work to shed weight.

- Avoid F411 processors. They're near the end of useful life for Betaflight. F3 and F1 are already not supported, but you will sometimes find them for super cheap. Not worth your time.

- Stacks are cheaper than AIOs in the long run. You will blow up an ESC or flight controller, and replacing one of those is less expensive than replacing a whole AIO.

- Plastic frames outside of the 75mm class are more trouble than they're worth. Carbon fiber or bust when getting bigger.

- Ignore FrSky and Spektrum. Get an ExpressLRS radio, or module if you already have the radio.

- Get some time in a decent simulator like Velocidrone or Liftoff before putting your first quad in the air. It'll save you time and money on broken parts, and has a reset button to make your first tens of crashes easier to recover from.
