## Should I use FrSky, Flysky, or Spektrum control protocols?

No.

Protocols by FrSky, Flysky, and Spektrum all belong to earlier generations of spread-spectrum, digital control links. These have worse performance and smaller feature sets than any of the current-generation control links, such as ExpressLRS, TBS Crossfire, and ImmersionRC Ghost.

Further, FrSky's firmware ecosystem is notoriously complex.
