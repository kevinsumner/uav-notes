# HDZero

## HDZero and Shark Byte Hardware
[Shark Byte Manual](https://s3.amazonaws.com/helpscout.net/docs/assets/52a0a907e4b010488044ba40/attachments/606fd1bc8af76a714bfd577e/Shark-Byte-User-Manual-08042021.pdf) (Includes TX5M.1 and TX5S.1)
[TX5R.1 (aka HDZero Race) Manual](https://28003cc0-88ab-4618-8fa6-9747a051df97.filesusr.com/ugd/967e02_c2c069de37f84ed0b883e0035ff3e0bb.pdf)

|                              | **TX5M.1** | **TX5R.1** | **TX5S.1** |
| ---------------------------- | ---------- | ---------- | ---------- |
| **External Dimensions (mm)** | 25x32x12   | 45x27      | 32.5x32.5  |
| **Mounting Dimensions (mm)** | 20x20      | 20x20      | 25.5x25.5  |
| **Antenna Connector**        | MMCX       | u.FL       | u.FL       |
| **Weight w/o hardware (g)**  | 10.8       | 6.8        | 5.8        |
| **Max Output (mW)**          | 500        | 200        | 200        |
| **Min Voltage (S)**          | 2          | 2          | 2          |
| **Max Voltage (S)**          | 6          | 6          | 6          |



### Shark Byte TX5S.1
![Fat Shark Shark Byte TX5S.1.png](pinouts/Fat_Shark_Shark_Byte_TX5S.1.png)
![Fat Shark Shark Byte TX5S.1 - diagram.png](pinouts/Fat_Shark_Shark_Byte_TX5S.1_-_diagram.png)


### HDZero Freestyle VTX
Wiring harness order (left to right looking at plug):
- SmartAudio (originally blue)
- FC TX (originally green)
- FC RX (originally yellow)
- Ground (originally black)
- Voltage (originally red)


### Tips
#### Prevent deletion of SD card files after upgrade
Donotremove.txt

#### Upgrade RunCam HD Nano with M12 lens for better clarity
https://www.youtube.com/watch?v=Up-GMiEJA8w
https://www.youtube.com/watch?v=E1BqN_Ttv44&

#### Shark Byte OSD
https://www.youtube.com/watch?v=hHu7HkH7rpM

If using UART3 for Shark Byte VTx, command-line serial is 2 (subtract 1 from UART number).

```
set osd_displayport_device = MSP
set displayport_msp_serial = 2
```

