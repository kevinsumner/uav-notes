## What control (RC) protocol should I start with?

ExpressLRS on the 2.4GHz band.

ExpressLRS is an inexpensive, long range, and modern protocol with an active open source project and passionate developers behind it. It is suitable for everything from the smallest tiny whoops up to ultra long range, and everything in between. It has the fastest packet rate and lowest latency of every protocol currently available.

ExpressLRS supports both the 2.4GHz band and the 900MHz band. The 2.4GHz band has the advantage here by being wider, meaning more possible pilots in the air and more space to steer around external interference, while getting tens-of-kilometers of range in open air on modest power outputs. It also has much smaller and lighter antennas than those needed for the 900MHz band. The band is essentially the same world-wide, unlike the 900MHz band, which is actually 915MHz in some places and 865MHz in others.

The featureset of ExpressLRS is a near-superset of all other control protocols. The one notable exception is it's lack of an abililty to flash receivers over the RC link. Practically, this is mitigated by Wifi-based flashing, although it is not quite as seemless of an experience.

The largest difficulty with ExpressLRS is distinguishing good hardware from bad. Much like Betaflight flight controllers, vendors of ExpressLRS vary greatly in the quality of hardware they produce. In general, RadioMaster and HappyModel have been praised by the ExpressLRS developers for their willingness to work together and deliver exceptionally-good hardware. Each has had missteps, but in those cases they have made it right with their customers.
