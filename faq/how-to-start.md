## How should I start into FPV?

Get a decent radio, such as the RadioMaster Zorro, RadioMaster Boxer, RadioMaster TX12, or Jumper T-Pro, and start in a simulator, like Velocidrone or Liftoff.

In between flight sessions, start researching what flight styles you might want to try (e.g., racing, freestyle, and long range) and, generally, begin learning about FPV generally. Specifically, look into these things:

- How to maintain lithium-based batteries, specifically LiPo batteries
- How to solder
- What basic components are in an FPV aircraft and how they are connected

Before your first flight with a physical quad, and especially before launching a bigger quad, go complete the relevant safety testing, licensing, and registration. In the US, that means:

- Taking the [FAA TRUST safety test](https://www.faa.gov/uas/recreational_flyers/knowledge_test_updates).
- Registering at the [FAA Drone Zone](https://faadronezone.faa.gov/).
- Selecting and reading a set of flight safety guidelines, such as those from the [FPV Freedom Coalition](https://fpvfc.org/safety-guidelines).
- Getting a HAM radio operator license to comply with FCC regulations for unlicensed RF transmitters.
- If doing any commercial work, even potentially including monetized YouTube videos, getting an [FAA Part 107 certification](https://www.faa.gov/uas/commercial_operators/become_a_drone_pilot).

For your first physical craft, get a 65mm or 75mm ducted quad ("tiny whoop"), such as a Mobula 6, Moblite 6, or Mobula 7. These can be flown safely indoors.

Decide whether you want to build or buy. Buying a bind-and-fly ("BNF") will get you off the ground quicker, but you will still need to learn how it's built because you'll need to repair it when you crash it. There are also more limited options with BNF instead of building.

Finally, for your first build, whether you bought a BNF before it or not, look to other BNFs in the size class and flight style you're interested in to decide about specs, like motor size and KV. Put a build list together on a site like [Rotor Builds](https://rotorbuilds.com) or [Quad Part Picker](https://www.quadpartpicker.com/) to help you ensure you have all the necessary parts. Optionally, share the link to the build list and request feedback from one of the various communities, such as /r/fpv on Reddit or on one of the FPV-centric Discord servers.
