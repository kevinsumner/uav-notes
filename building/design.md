# Designing a Multicopter

## Decide on style
- Freestyle
- Racing
- Long range
- Cinewhoop
- Bashing
- ...

## Decide on frame size class
- Mini: 6"/5"/4"/3"
- Micro: 2.5"/95mm/85mm/65mm
- Large: 7", Beast, X, etc.

## Decide on FPV video system
- Analog
- Shark Byte
- DJI

### Analog Considerations
- Using SmartAudio or Tramp? Needs a UART Rx.
- Using TBS VTx? Consider wiring directly from TBS Rx to VTx.

### Shark Byte Considerations
FC needs one spare UART with both Rx and Tx.

### DJI Considerations
FC needs to have spare UARTs and may need to provide 10V.

## Decide on flight controller and ESC configuration
- All-in-one FC/ESC combination
- 4-in-1 ESC with separate FC
- Individual ESCs, power distribution board, separate FC
- Niche options (hexacopter, octocopter, etc.)

[Toothpick/Whoop Flight controllers](http://bit.ly/Whoop-Toothpick-Flight-Controller-list-spreadsheet)

## Decide on voltage/battery cell count
[Battery performance table maintained by Joshua Bardwell](https://docs.google.com/spreadsheets/u/0/d/10-0JsOiuIFB6GCJF4rX_foSdA9Cx5UnjYR3UlZa7gA4/htmlview)


## Decide on control link
- ExpressLRS
- TBS Crossfire
- TBS Tracer
- FrSky ACCESS
- Spektrum DSMX
- and many more...

## Pick and buy parts
- [ ] Frame
- [ ] Flight Controller or AIO
- [ ] ESC(s) (if needed)
- [ ] PDB (if needed)
- [ ] Rx
- [ ] Rx Antenna
- [ ] VTX
- [ ] VTX Antenna
- [ ] Camera
- [ ] Motors
- [ ] Motor screws
- [ ] Props
- [ ] Batteries
- [ ] Battery straps
- [ ] Pigtail for battery/ESC
- [ ] Capacitor and/or TVS diodes (e.g. Fettec Spike Absorber)
- [ ] Stack screws/nuts/standoffs
- [ ] Damping/locking compounds (Loc-tite, NyoGel, etc.)
- [ ] Motor disconnect plugs (MR30, MT30, etc.)
