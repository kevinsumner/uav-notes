Post Build Check List
---------------------

- [ ] Update Betaflight
- [ ] Update VTX firmware
- [ ] Bind Reciever
- [ ] Check gyro orientation
- [ ] Set up Betaflight ports
- [ ] Set up Betaflight aux modes
- [ ] Set up Betaflight OSD
- [ ] If needed, set up OSD MSP connection
- [ ] If needed, configure VTX table
- [ ] Set up Betaflight rates
- [ ] Set up RPM filtering
- [ ] Enable bidirectional DSHOT
- [ ] Check motor location, direction, and errors
