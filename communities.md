# Communities

## Discord

### General
- [/r/fpv's Discord](https://discord.gg/9hpsd2G)
- [Drone Community Discord](https://discord.com/invite/rdDTQrw)
- [/r/radiocontrol Discord](https://discord.gg/6RUrwna), in which  /r/Multicopter has a channel.

### Technology-Focused
- [Betaflight Discord](https://discord.betaflight.com/invite)
- [ExpressLRS Discord](https://discord.gg/dS6ReFY)
- [HDZero Discord](https://discord.gg/kGsnEDMb2V)
- [Bluejay](https://discord.gg/phAmtxnMMN)
- [ESC Configurator](https://discord.gg/QvSS5dk23C) (also has some AM32 and Bluejay discussion)

### Public
- [CiottiFPV's Discord](https://discord.gg/c6aufkk) (also has Patreon-only channels)
- [FPV Exchange Discord](https://discord.gg/QdrKbRMH)
- [Chris Rosser's Discord](https://discord.gg/Fb3HuxtuSf)
- [Mark Spatz/UAV Tech Discord](https://discordapp.com/invite/rCCzgeT) (tuning-focused)

### Private -- Requires Membership
- Joshua Bardwell's Discord [via Patreon](https://www.patreon.com/bePatron?c=324734)
- Nick Burn's Discord [via Patreon](https://www.patreon.com/nickburnsrc)
- Mad Tech Discord [via Patreon](https://www.patreon.com/MadRC)
