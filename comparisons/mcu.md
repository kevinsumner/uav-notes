# MCU Comparison

* Last updated 2023-10-16. Current Betaflight release is 4.4.2.

| Name          | Last Betaflight Version | Frequency (MHz) | RAM (KB) | Flash (KB) | Dimensions (mm) |
| ------------- | ----------------------- | --------------- | -------- | ---------- | --------------- |
| AT32F435CGU7  | 4.5.0 (expected)        | 288             | 512      | 1024       |                 |
| STM32F103C8T6 | 3.2.5                   | 72              | 20       | 64         |                 |
| STM32F303CCT6 | 4.0.6                   | 72              | 40       | 256        |                 |
| STM32F405RGT6 | Current\*               | 168             | 192      | 1024       |                 |
| STM32F411CEU6 | Current\*               | 100             | 128      | 512        | 7x7             |
| STM32F722RET6 | Current\*               | 216             | 256      | 512        |                 |
| STM32F745VGH6 | Current\*               | 216             | 320      | 1024       |                 |
| STM32G473CEU6 | Current\*               | 170             | 128      | 512        |                 |
| STM32H743VIT6 | Current\*               | 480             | 1024     | 2048       |                 |
