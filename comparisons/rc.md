# Remote Control Link Comparison

|                                   | ExpressLRS 2.4GHz            | ExpressLRS 900MHz                                         | TBS Crossfire               | TBS Tracer               | ImmersionRC Ghost                 | FrSky ACCST D16 |
| --------------------------------- | ---------------------------- | --------------------------------------------------------- | --------------------------- | ------------------------ | --------------------------------- | --------------- |
| RF Band                           | 2.4GHz                       | 900MHz                                                    | 900MHz                      | 2.4GHz                   | 2.4GHz                            | 2.4GHz          |
| Maximum Packet Rate               | 500Hz (1000Hz<sup>pre</sup>) | 200Hz                                                     | 150Hz                       | 250Hz                    | 500Hz                             |                 |
| Maximum Power                     | 1000mW                       | 1000mW                                                    | 2000mW                      | 1000mW                   | 350mW                             |                 |
| Maximum Control Channels          | 12                           | 12                                                        | 16                          | 16                       | 12                                |                 |
| Supported Modulations             | LoRa, FLRC<sup>pre</sup>     | LoRa, FLRC<sup>pre</sup>                                  | LoRa, GSK<sup>tbv</sup>     | FLRC, LoRa<sup>pre</sup> | LoRa, MSK                         |                 |
| Maximum Verified Range            | 35km @ 100mW<sup>dnf</sup>   | 40.1km @ 10mW<sup>dnf</sup><br/>30.1km @ 1W<sup>dnf</sup> | 32.6km @ 25mW<sup>dnf</sup> | 5.4km @ 100mW            | 24.1km @ 100mW<br/>27.5km @ 350mW |                 |
| Lowest Average Measured Latency   | 2.0ms @ 500Hz, telemetry off | 5.0ms @ 200Hz, telemetry 1:128                            | 6.6ms @ 150Hz, telemetry on | 4.0ms, telemetry off     | 2.1ms @ 500Hz, telemetry off      |                 |
| Update Over RC Link?              | No                           | No                                                        | Yes                         | Yes                      | Yes                               |                 |
| Update via wifi/Bluetooth?        | Yes                          | Yes                                                       | No                          | No                       | No                                |                 |
| Update via Betaflight Passthrough | Yes                          | Yes                                                       | No<sup>tbv</sup>            | No<sup>tbv</sup>         | No                                |                 |
| Repeated Packet Mode?             | Yes<sup>pre</sup>            | No                                                        | No                          | No                       | Yes                               |                 |
| Encryption?                       | No                           | No                                                        | Yes<sup>pre</sup>           | No                       | No                                | No              |
| VTX Control?                      | Yes                          | Yes                                                       | Yes                         | Yes                      | Yes                               | No              |

<sup>tbv</sup> To be verified

<sup>pre</sup> Pre-release code

<sup>dnf</sup> Did not failsafe; max range undetermined

## Sources

[Wezley Varty: Happy Model 2.4G ExpressLRS - Range test + Review](https://www.youtube.com/watch?v=dBmTRhgVcyYt=696)

[Wezley Varty: Ghost Range Test - It went so far I lost the wing!](https://youtu.be/spYKBfVqYTQ?t=698)

[Wezley Varty: ExpressLRS 99LQ at 30kms out!!](https://youtu.be/SbWvFIpVkto?t=139)

[Wezley Varty: Happy Model ExpressLRS 900Mhz Hardware - Range Test (Plus crossfire comparison)](https://www.youtube.com/watch?v=0QWN9qWoSYY)

[Mark Spatz: RC Control Modules: End-to-End & Packet Jitter (v3, 2021-12-27)](https://theuavtech.com/wp-content/uploads/2021/12/2021.12.27-RC-Link-Latency-Testing-V3-UAVtech-Data.pdf)


