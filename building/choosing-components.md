# Choosing Components

## Flight Controller

## Motors

Highlights:

- Propeller tip speeds of >0.6 mach causes efficiency losses, >0.8 mach causes vibrations that are difficult to tune.
  - Kv rating is unloaded, i.e. without a prop. Multiply by 0.7 (70%) when calculating loaded tip speed for a reasonable approximation.
- Stator volume is the leading indicator of torque. Magnets, number of windings, and windings' wire gauge also come into effect.
- Ratio of stator diameter to height indicates cooling ability of the motor. 3:1 diameter to height is optimal, but may need to move to 4:1 or even 5:1 depending on flight style.

https://www.youtube.com/watch?v=Y8HYoRXBQW4&t=1245s
