Tools and Supplies
==================

This list of tools is specifically skewed towards high quality items, when available. This means cheaper options may exist, but are likely also inferior in construction or lack some desirable feature.

Items marked † are sufficent, but higher quality replacements are being sought out.

## On the bench
Tool kit: iFixit Pro Tech Toolkit ([iFixit](https://www.ifixit.com/Store/Tools/Pro-Tech-Toolkit/IF145-307))
Crosslock tweezer set: ([McMaster-Carr](https://www.mcmaster.com/7007A81/))
Side-cut snips: Knipex Super Knips, 5" ([Amazon](https://smile.amazon.com/gp/product/B0048F601Q))
Soldering iron: Hakko FX-888D ([Adafruit](https://www.adafruit.com/product/1204))
Soldering tip: Hakko D18-T16 Screwdriver Tip ([Adafruit](https://www.adafruit.com/product/1250))
Solder: Kester 24-6337-8800 63% Tin, 37% Lead, No-Clean Flux Core ([Digi-Key](https://www.digikey.com/en/products/detail/kester-solder/24-6040-0018/365512)
Soldering flux: 
Soldering mat: Kaisi S-180 Silicone Mat ([Race Day Quads](https://www.racedayquads.com/products/s-180a1-large-heat-resistant-silicone-soldering-work-mat-w-magnets))
Multimeter: Fluke 117 Multimeter ([Amazon](https://www.amazon.com/gp/product/B000O3LUEI))

## In the flight bag
USB Cable: Nomad Universal Cable USB-C Kevlar ([Nomad Goods](https://nomadgoods.com/products/universal-usb-c-kevlar-cable))
Tool kit†: RDQ 9 Piece Drone Tool Kit v2 ([Race Day Quads](https://www.racedayquads.com/products/rdq-9-piece-drone-racing-tool-kit?_pos=1&_sid=385a9f824&_ss=r))
Soldering iron: Pine64 Pinecil ([Pine64](https://pine64.com/product/pinecil-smart-mini-portable-soldering-iron/))
Soldering tip: Pinecil Soldering Tip Set (specifically, TS-D24 Screwdriver Tip) ([Pine64](https://pine64.com/product/pinecil-soldering-tip-set-gross/))
Soldering iron cleaner: Aoyue Soldering Iron Tip Cleaner with Brass wire sponge ([Amazon](https://www.amazon.com/gp/product/B005C789EU))
