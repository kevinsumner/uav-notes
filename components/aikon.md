# Aikon

## AK32Pro 4-in-1 50A 6S ESC
![Aikon_AK32Pro_4-in-1_50A_6S.png](pinouts/Aikon_AK32Pro_4-in-1_50A_6S.png)

## F7 Mini v3
![Aikon_F7_Mini_v3.png](pinouts/Aikon_F7_Mini_v3.png)

## F7 v2
![Aikon_F7_v2.png](pinouts/Aikon_F7_v2.png)
