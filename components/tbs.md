# TBS Crossfire

## Crossfire Nano Rx
![TBS_Crossfire_nano_Rx.png](pinouts/TBS_Crossfire_nano_Rx.png)
Channel 1 defaults to UART Tx (connect to FC UART Rx).
Channel 2 defaults to UART Rx (connect to FC UART Tx).

[Manual](https://www.team-blacksheep.com/tbs-crossfire-nano-quickstart.pdf)

### Sixty9 Crossfire
![TBS_Sixty9_Crossfire_-_plug_version.png](pinouts/TBS_Sixty9_Crossfire_-_plug_version.png)
