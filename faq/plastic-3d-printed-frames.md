## Are plastic frames (3D printed or moulded) good for FPV?

Not really. Plastics are not nearly as stiff or strong as layered carbon fiber. This means vibrations from the props spinning, motors spinning, and frame bending will resonate, making the flight controller less effective at detecting actual movement of the craft.

