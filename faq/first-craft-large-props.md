### Should I get a craft with >5" propellers as my first craft?

In short, _ABSOLUTELY NOT_.

Larger propellers carry more inertia than smaller propellers. Even 3" propellers can send someone to the hospital for stitches. 5" can do serious damage, and 10" can kill a person. The risk to objects, like buildings, cars, and power lines, increases similarly.

This isn't an exaggeration. As an example, Catalyst Machineworks produces frames in the "macro" class. Here is their product safety warning for their crafts in this class:

https://cdn.shopify.com/s/files/1/1761/2751/files/PRODUCT_SAFETY_WARNING_CANNONBALL_AND_TASMANIAN_FRAMES.pdf

Beyond that, larger quad requires expert-level skill to both fly and tune. This can make the experience far more challenging and significantly reduce enjoyment for someone just starting out.
