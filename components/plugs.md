# Plugs

Common plug types and their usages in RC applications listed below.

## 2mm bullet
Exposed, usually-brass connector

20A max continuous, 40A burst

## BT2.0

9A continuous, 15A burst

## GNB27

9A continuous, 15A burst

## JST PH 1.25
White plug commonly found on ~125mAh 1S batteries, e.g., for UMX-class planes and helicopters

## JST PH 2.0
White plug commonly found on ~250mAh 1S batteries, e.g., for 65mm whoops

5A max continuous, 10A burst

## JST RCY
Red plug commonly found on 1S and 2S batteries, more frequently in planes

5A max continuous, 10A burst

## JST SH 1.0
White plug commonly found on:
- FC to 4-in-1 ESC connections, usually 8 pins
- DJI Air unit to FC, 8 pins

1A rated continuous

## MMCX
Coaxial cable connector for antenna connections

## MR30
Usually yellow plug used as a brushless motor quick connect

15A rated continuous, 30A max continuous

## RP-SMA
Coaxial cable connector for antenna connections

"RP" stands for "reverse polarity". This is the same as SMA, except the
pin is on the screw side.

## SMA
Coaxial cable connector for antenna connections

Pin is on the nut side. See RP-SMA.

## U.FL aka I-PEX
Coaxial cable connector for antenna connections

Female (cable) side usually terminates at a right-angle to the cable.

## XT30
Power lead for micro quads, e.g., 85mm whoop and 3 inch micro quads

Generally used with 18 AWG wire

15A rated continuous, 30A max continuous, 60A burst

## XT60
Power leads on mini quads

Generally used with 14 or 12 AWG wire

30A rated continuous, 60A max continuous, 120A burst
